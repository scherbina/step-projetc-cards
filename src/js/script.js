import {SectionHeader} from './modules/Section/Header.min.js'
import {SectionFilter} from './modules/Section/Filter.min.js'
import {SectionDesk} from './modules/Section/Desk.min.js'

// import {SectionHeader} from './modules/Section/Header.js'
// import {SectionFilter} from './modules/Section/Filter.js'
// import {SectionDesk} from './modules/Section/Desk.js'


class CardsApp {
    constructor() {
        this._init();
        this._boundListeners();
        this._createHeaderUI();
        this._createFilterUI();
        this._createDeskUI();
    }

    _init() {
        this._container = document.createElement('div');
        this._container.classList.add('cardsApp');
        this._data = {};

        const token = localStorage.getItem('tokenCard');
        token ? this._data.token = token : this._data.token = null;
    }

    _boundListeners() {
        this._data.onLoginSuccess = this._onLoginSuccess.bind(this);
        this._data.onGetCardsSuccess = this._onGetCardsSuccess.bind(this);
        this._data.onCardCreated = this._onNewCardCreated.bind(this)
    }

    _onLoginSuccess(token) {
        localStorage.setItem('tokenCard', token);
        this._data.token = token;
        this._data.sectionFilter.enableFilter();
        this._data.sectionDesk.getVisits(token);
    }

    _onGetCardsSuccess(response) {
        this._data.sectionFilter.enableFilter();
        this._data.sectionDesk._createDeskUI(response);
    }

    _onNewCardCreated(card) {
        if(card.id) this._data.sectionDesk._createSingleCard(card);
    }

    appendTo(parent){
        parent.append(this._container);
    }

    _createHeaderUI() {
        const sectionHeader = new SectionHeader(this._data);
        this._container.append(sectionHeader.getContainer());
        // this._sectionHeader = sectionHeader;
        this._data.sectionHeader = sectionHeader;
    }

    _createFilterUI() {
        const sectionFilter = new SectionFilter(this._data);
        this._container.append(sectionFilter.getContainer());
        // this._sectionFilter = sectionFilter;
        this._data.sectionFilter = sectionFilter;
    }

    _createDeskUI() {
        const sectionDesk = new SectionDesk(this._data);
        this._container.append(sectionDesk.getContainer());
        // this._sectionDesk = sectionDesk;
        this._data.sectionDesk = sectionDesk;
    }
}

const app = new CardsApp();
app.appendTo(document.body);

export {app}