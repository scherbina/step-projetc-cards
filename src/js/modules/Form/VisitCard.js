
import {Component} from "../Base/Component.min.js";
import {remoteDataExchange} from "../Base/AJAX.min.js";
import {Modal} from "../Base/Modal.min.js";
import {EditForm} from "../Form/Edit.min.js";

// import {Component} from "../Base/Component.js";
// import {remoteDataExchange} from "../Base/AJAX.js";
// import {Modal} from "../Base/Modal.js";
// import {EditForm} from "../Form/Edit.js";

class VisitCard extends Component {

    static props = {
        // cardsURL:'http://cards.danit.com.ua/cards/',
        class: {
            container: 'cardsApp_patientCard',
            containerMainPart: 'patientCard-containerMainPart',
            editButton: 'patientCard-editBtn',
            deleteButton: 'patientCard-deleteBtn',
            expandButton: 'patientCard-expandBtn',
            patientCardButtonsContainer: 'patientCard-Btns',
            rowContainer: 'patientCard-row',
            rowTitle: 'patientCard-row-title',
            rowText: 'patientCard-row-text',
            hiddenFields: 'patientCard-hiddenFields',
            hiddenFieldsVisible: 'patientCard-hiddenFieldsNotVisible'
        }
    }

    constructor(inputDataObject, appData) {
        super();
        this._inputs = inputDataObject;
        this._cardUI();
        this._addListener();
        this._hiddenFields;
        this._token = appData.token;
    }

    _valueTranslate(value) {
        let valueRus;
        switch (value) {
            case 'Cardiologist':
                valueRus = 'Кардиолог';
                break
            case 'Dentist':
                valueRus = 'Стоматолог';
                break
            case 'Therapist':
                valueRus = 'Терапевт';
                break
            case 'regular':
                valueRus = 'обычная';
                break
            case 'priority':
                valueRus = 'приоритетная';
                break
            case 'urgent':
                valueRus = 'неотложная';
                break
        }
        return valueRus;
    }

    _cardUI() {

        // --- containers
        this._container = document.createElement('div');
        this._container.classList.add(VisitCard.props.class.container);

        this._containerMainPart = document.createElement('div');
        this._containerMainPart.classList.add(VisitCard.props.class.containerMainPart);
        this._container.append(this._containerMainPart);

        this._hiddenFields = document.createElement('div');
        this._hiddenFields.classList.add(VisitCard.props.class.hiddenFields);
        this._container.append(this._hiddenFields)

        this._patientCardButtonsContainer = document.createElement('div');
        this._patientCardButtonsContainer.classList.add(VisitCard.props.class.patientCardButtonsContainer);
        this._container.append(this._patientCardButtonsContainer)

        // --- buttons

        this._editButton = document.createElement('span');
        this._editButton.classList.add(VisitCard.props.class.editButton);
        this._editButton.innerText = 'Edit';

        this._deleteButton = document.createElement('span');
        this._deleteButton.classList.add(VisitCard.props.class.deleteButton);
        this._deleteButton.innerText = 'Delete';

        this._expandButton = document.createElement('span');
        this._expandButton.classList.add(VisitCard.props.class.expandButton);
        this._expandButton.innerText = 'Expand';

        // --- add buttons in container

        this._patientCardButtonsContainer.append(this._expandButton);
        this._patientCardButtonsContainer.append(this._editButton);
        this._patientCardButtonsContainer.append(this._deleteButton);

        // To fill out main part container

        this._containerMainPart.insertAdjacentHTML('beforeend', `              
           
                <div class="${VisitCard.props.class.rowContainer}">
                    <p class="${VisitCard.props.class.rowTitle}">Номер карты</p>
                    <p class="${VisitCard.props.class.rowText} id">${this._inputs.id}</p>
                </div>                 
                <div class="${VisitCard.props.class.rowContainer}">
                    <p class="${VisitCard.props.class.rowTitle}">Специализация Врача</p>
                    <p class="${VisitCard.props.class.rowText} doctor">${this._valueTranslate(this._inputs.doctor)}</p>
                </div>    
                <div class="${VisitCard.props.class.rowContainer}">
                    <p class="${VisitCard.props.class.rowTitle}">ФИО</p>
                    <p class="${VisitCard.props.class.rowText} fullname">${this._inputs.fullname}</p>
                </div>                         
        
        `)

        this._hiddenFields.insertAdjacentHTML('beforeend', `           

                <div class="${VisitCard.props.class.rowContainer}">
                    <p class="${VisitCard.props.class.rowTitle}">Цель Визита</p>
                    <p class="${VisitCard.props.class.rowText} purpose">${this._inputs.purpose}</p>
                </div>
                <div class="${VisitCard.props.class.rowContainer}">
                    <p class="${VisitCard.props.class.rowTitle}">Описание Жалобы</p>
                    <p class="${VisitCard.props.class.rowText} description">${this._inputs.description}</p>
                </div>   
                 <div class="${VisitCard.props.class.rowContainer}">
                    <p class="${VisitCard.props.class.rowTitle}">Срочность</p>
                    <p class="${VisitCard.props.class.rowText} urgency">${this._valueTranslate(this._inputs.urgency)}</p>
                </div>  
        `)
    }

    _addListener() {
        this._onClickExpandButtonBound = this._onClickExpandButton.bind(this);
        this._onClickDeleteButtonBound = this._onClickDeleteButton.bind(this);
        this._onClickEditButtonBound = this._onClickEditButton.bind(this);
        this._onEditFormClosedBound = this._onEditFormClosed.bind(this);

        this._expandButton.addEventListener('click', this._onClickExpandButtonBound);
        this._deleteButton.addEventListener('click', this._onClickDeleteButtonBound);
        this._editButton.addEventListener('click', this._onClickEditButtonBound);

    }

    _removeListeners() {
        this._expandButton.removeEventListener('click', this._onClickExpandButtonBound);
        this._deleteButton.removeEventListener('click', this._onClickDeleteButtonBound);
        this._editButton.removeEventListener('click', this._onClickEditButtonBound);
    }

    _onClickExpandButton() {
        this._hiddenFields.classList.toggle(VisitCard.props.class.hiddenFieldsVisible);
    }

    _onClickDeleteButton() {
        remoteDataExchange({
            method: 'DELETE',
            token: this._token, 
            id: this._inputs.id, 
            onSuccess: this.destroy.bind(this)
        })
    }

    _onClickEditButton() {
        this._editForm = new Modal(
            new EditForm(this._token, this._onEditFormClosedBound, this._inputs)
        )
        this._editForm.open();
    }

    _onEditFormClosed() {
        this._editForm.destroy();
    }

}

export {VisitCard}
