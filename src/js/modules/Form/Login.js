import {Form} from "../Base/Form.min.js";
import {remoteDataExchange} from "../Base/AJAX.min.js";

// import {Form} from "../Base/Form.js";
// import {remoteDataExchange} from "../Base/AJAX.js";


class LoginForm extends Form {
    static props = {
        class: {
            container: 'cardsApp_formLogin',
        }
    }

    constructor(onLoginSuccess, onLoginCancel) {
        super();
        this._container.classList.add(LoginForm.props.class.container);
        this._onLoginSuccessToApp = onLoginSuccess;
        this._onLoginCancelToApp = onLoginCancel;
        this._boundListeners();
        this._createUI();
    }

    _boundListeners() {
        this._onOKBtnClickBound = this._onOKBtnClick.bind(this);
        this._closeBound = this.close.bind(this);
    }

    _createUI() {
        this.addControl('input', {
            value: '',
            htmlAttr: [
                {   
                    key: 'placeholder',
                    value: 'Введите email'
                },
                {
                    key: 'id',
                    value: 'loginFormEmail'
                },
                {
                    key: 'name',
                    value: 'loginFormEmail'
                },
                {
                    key: 'autocomplete',
                    value: 'username'
                }
            ],
            label: {
                text: 'E-mail:',
            }
        });
        this.addControl('input', {
            value: '',
            htmlAttr: [
                {   
                    key: 'placeholder',
                    value: 'Пароль'
                },
                {
                    key: 'id',
                    value: 'loginFormPassword'
                },
                {
                    key: 'name',
                    value: 'loginFormPassword'
                },
                {
                    key: 'type',
                    value: 'password'
                },
                {
                    key: 'autocomplete',
                    value: 'current-password'
                }
            ],
            label: {
                text: 'Пароль:',
            }
        });
        this.addControl('button', {
            text: 'Войти',
            listeners: [
                {
                    type: 'click',
                    cb: this._onOKBtnClickBound
                },
            ],
        });
        this.addControl('button', {
            text: 'Отмена',
            listeners: [
                {
                    type: 'click',
                    cb: this._closeBound
                },
            ],
        });
    }

    _onOKBtnClick() {
        const email = this._container.loginFormEmail.value;
        const password = this._container.loginFormPassword.value;

        remoteDataExchange({
            method: 'POST',
            onSuccess: this._onLoginSuccess.bind(this),
            body: JSON.stringify({email, password})
        })
    }

    close() {
        this.destroy();
        this._onLoginCancelToApp();
    }

    _onLoginSuccess(response) {
        this.destroy();
        this._onLoginSuccessToApp(response);
    }
}

export {LoginForm};