import {VisitCard} from "../Form/VisitCard.min.js";


// import {VisitCard} from "../Form/VisitCard.js";

export class VisitDentist extends  VisitCard {
    constructor(inputDataObject, appData) {
        super(inputDataObject, appData);

        this._toFillSpecificFields()

    }

    _toFillSpecificFields() {

        this._hiddenFields.insertAdjacentHTML('beforeend', `        

                <div class="${VisitCard.props.class.rowContainer}">
                    <p class="${VisitCard.props.class.rowTitle}">Дата последнего посещения</p>
                    <p class="${VisitCard.props.class.rowText} lastVisit">${this._inputs.lastVisit}</p>
                </div>                          
        
        `)
    }
}
