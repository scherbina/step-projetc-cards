import {Form} from "../Base/Form.min.js";
import * as FormControl from "../Base/FormControls.min.js";
import {app} from "../../script.min.js";
import {remoteDataExchange} from "../Base/AJAX.min.js";

// import {Form} from "../Base/Form.js";
// import * as FormControl from "../Base/FormControls.js";
// import {app} from "../../script.js";
// import {remoteDataExchange} from "../Base/AJAX.js";


class EditForm extends Form {
    static props = {
        createURL: 'http://cards.danit.com.ua/cards',
        class: {
            container: 'cardsApp_formCreate',
        }
    }

    constructor(token, onCancaled, editInputs) {
        super();
        
        this._editInputs = editInputs;
        this._container.classList.add(EditForm.props.class.container);
        this._onCanceledToApp = onCancaled;
        this._token = token;
        this._boundListeners();
        this._createUI();
        this._onDoctorChangedBound();
        this._addFormInitvalues(this._editInputs);
    }

    _boundListeners() {
        this._onDoctorChangedBound = this._onDoctorChanged.bind(this);
        this._closeBound = this.close.bind(this);
        this._onCreateBtnClickBound = this._onCreateBtnClick.bind(this);
    }

    _createUI() {
        this.doctorLabel = this.addControl('label', {
            text: 'Выбор врача:',
            htmlFor: 'createFormDoctor',
        });

        this._doctor = this.addControl('select', {
            options: [
                {
                    text: '',
                    value: 'empty',
                },
                {
                    text: 'Кардиолог',
                    value: 'Cardiologist',
                },
                {
                    text: 'Стоматолог',
                    value: 'Dentist',
                },
                {
                    text: 'Терапевт',
                    value: 'Therapist',
                },
            ],
            htmlAttr: [
                {
                    key: 'id',
                    value: 'createFormDoctor'
                },
                {
                    key: 'name',
                    value: 'createFormDoctor'
                },
                {
                    key: 'required',
                    value: 'true'
                }
            ],
            listeners: [
                {
                    type: 'change',
                    cb: this._onDoctorChangedBound
                },
            ],
            class: ['form-control']

        });

        this._purposeLabel = this.addControl('label', {
            text: 'Цель визита:',
            htmlFor: 'createFormPurpose',
            class: ['hide']
        });

        this._purpose = this.addControl('textarea', {
            value: '',
            htmlAttr: [
                {   
                    key: 'placeholder',
                    value: 'Цель визита'
                },
                {
                    key: 'id',
                    value: 'createFormPurpose'
                },
                {
                    key: 'name',
                    value: 'createFormPurpose'
                },
                {
                    key: 'cols',
                    value: '100'
                },
                {
                    key: 'rows',
                    value: '2'
                },
                {
                    key: 'required',
                    value: 'true'
                }
            ],
            class: ['hide', 'form-control']
        });

        this._descriptionLabel = this.addControl('label', {
            text: 'Описание визита:',
            htmlFor: 'createFormDescription',
            class: ['hide']
        });

        this._description = this.addControl('textarea', {
            value: '',
            htmlAttr: [
                {   
                    key: 'placeholder',
                    value: 'Описание визита'
                },
                {
                    key: 'id',
                    value: 'createFormDescription'
                },
                {
                    key: 'name',
                    value: 'createFormDescription'
                },
                {
                    key: 'cols',
                    value: '100'
                },
                {
                    key: 'rows',
                    value: '5'
                },
            ],
            class: ['hide', 'form-control']

        });

        this._urgencyLabel = this.addControl('label', {
            text: 'Срочность:',
            htmlFor: 'createFormUrgency',
            class: ['hide']
        });

        this._urgency = this.addControl('select', {
            options: [
                {
                    text: 'Обычная',
                    value: 'regular',
                },
                {
                    text: 'Приоритетная',
                    value: 'priority',
                },
                {
                    text: 'Неотложная',
                    value: 'urgent',
                },
            ],
            htmlAttr: [
                {
                    key: 'id',
                    value: 'createFormUrgency'
                },
                {
                    key: 'name',
                    value: 'createFormUrgency'
                },
                {
                    key: 'required',
                    value: 'true'
                }
            ],
            class: ['hide', 'form-control']
        });

        this._fsPressure = this.addControl('fieldset', {
            text: 'Нормальное давление'
        });
        this._fsPressure.getContainer().classList.add('hide');

        this._fsPressureLowLabel = this._fsPressure.addControl(new FormControl.LabelComponent({
            text: 'Нижнее:',
            htmlFor: 'createFormNormalLowPressure'
        }));

        this._fsPressureLow = this._fsPressure.addControl(new FormControl.InputComponent({
            value: '',
            htmlAttr: [
                {
                    key: 'type',
                    value: 'number'
                },
                {
                    key: 'id',
                    value: 'createFormNormalLowPressure'
                },
                {
                    key: 'name',
                    value: 'createFormNormalLowPressure'
                },
                {
                    key: 'required',
                    value: 'true'
                }
            ],
            class: ['form-control'],
        }));
        
        this._fsPressureHighLabel = this._fsPressure.addControl(new FormControl.LabelComponent({
            text: 'Верхнее:',
            htmlFor: 'createFormNormalHighPressure'
        }));

        this._fsPressureHigh = this._fsPressure.addControl(new FormControl.InputComponent({
            value: '',
            htmlAttr: [
                {
                    key: 'type',
                    value: 'number'
                },
                {
                    key: 'id',
                    value: 'createFormNormalHighPressure'
                },
                {
                    key: 'name',
                    value: 'createFormNormalHighPressure'
                },
                {
                    key: 'required',
                    value: 'true'
                }
            ],
            class: ['form-control'],
        }));

        this._bmiLabel = this.addControl('label', {
            text: 'Индекс массы тела:',
            htmlFor: 'createFormBMI',
            class: ['hide']
        });

        this._bmi = this.addControl('input', {
            value: '',
            htmlAttr: [
                {
                    key: 'type',
                    value: 'number'
                },
                {
                    key: 'id',
                    value: 'createFormBMI'
                },
                {
                    key: 'name',
                    value: 'createFormBMI'
                },
                {
                    key: 'required',
                    value: 'true'
                }
            ],
            class: ['hide', 'form-control']
        });

        this._pastIllnessesnLabel = this.addControl('label', {
            text: 'Перенесенные заболевания:',
            htmlFor: 'createFormPastIllnessesn',
            class: ['hide']
        });

        this._pastIllnessesn = this.addControl('textarea', {
            value: '',
            htmlAttr: [
                {   
                    key: 'placeholder',
                    value: 'Перенесенные заболевания сердечно-сосудистой системы'
                },
                {
                    key: 'id',
                    value: 'createFormPastIllnessesn'
                },
                {
                    key: 'name',
                    value: 'createFormPastIllnessesn'
                },
                {
                    key: 'cols',
                    value: '100'
                },
                {
                    key: 'rows',
                    value: '5'
                },
            ],
            class: ['hide', 'form-control']
        });

        this._lastVisitLabel = this.addControl('label', {
            text: 'Дата последнего визита:',
            htmlFor: 'createFormLastVisit',
            class: ['hide']
        });
        this._lastVisit = this.addControl('input', {
            value: '',
            htmlAttr: [
                {
                    key: 'type',
                    value: 'date'
                },
                {
                    key: 'id',
                    value: 'createFormLastVisit'
                },
                {
                    key: 'name',
                    value: 'createFormLastVisit'
                },
                {
                    key: 'required',
                    value: 'true'
                }
            ],
            class: ['hide', 'form-control']
        });

        this._ageLabel = this.addControl('label', {
            text: 'Возраст:',
            htmlFor: 'createFormAge',
            class: ['hide']
        });
        this._age = this.addControl('input', {
            value: '',
            htmlAttr: [
                {
                    key: 'type',
                    value: 'number'
                },
                {
                    key: 'id',
                    value: 'createFormAge'
                },
                {
                    key: 'name',
                    value: 'createFormAge'
                },
                {
                    key: 'required',
                    value: 'true'
                }
            ],
            class: ['hide', 'form-control']
        });

        this._fullNameLabel = this.addControl('label', {
            text: 'ФИО:',
            htmlFor: 'createFormFullname',
            class: ['hide']
        });

        this._fullName = this.addControl('input', {
            value: '',
            htmlAttr: [
                {
                    key: 'type',
                    value: 'text'
                },
                {
                    key: 'id',
                    value: 'createFormFullname'
                },
                {
                    key: 'name',
                    value: 'createFormFullname'
                },
                {
                    key: 'required',
                    value: 'true'
                }
            ],
            class: ['hide', 'form-control']
        });

        this._btnCreate = this.addControl('button', {
            text: 'Сохранить',
            listeners: [
                {
                    type: 'click',
                    cb: this._onCreateBtnClickBound
                },
            ],
        });
        this._btnClose = this.addControl('button', {
            text: 'Закрыть',
            listeners: [
                {
                    type: 'click',
                    cb: this._closeBound
                },
            ],
        });
        
    }

    _onDoctorChanged() {
        let doctor
        this.getControlValue(this._doctor) === 'empty' ? doctor = this._editInputs.doctor : doctor = this.getControlValue(this._doctor);
        console.log(doctor);
        if (doctor !== 'empty') {
            this.show(this._purposeLabel);
            this.show(this._purpose);
            this.show(this._descriptionLabel);
            this.show(this._description);
            this.show(this._urgencyLabel);
            this.show(this._urgency);
            this.show(this._fullNameLabel);
            this.show(this._fullName);


            switch (doctor) {
                case 'Cardiologist': 
                    this.show(this._fsPressure);
                    this.show(this._bmi);
                    this.show(this._bmiLabel);
                    this.show(this._pastIllnessesn);
                    this.show(this._pastIllnessesnLabel);
                    this.show(this._ageLabel);
                    this.show(this._age);
                    this.hide(this._lastVisit);
                    this.hide(this._lastVisitLabel);
                    this._fsPressureLow.getContainer().required = true;
                    this._fsPressureHigh.getContainer().required = true;
                    this._bmi.getContainer().required = true;
                    this._pastIllnessesn.getContainer().required = true;
                    this._age.getContainer().required = true;
                    this._lastVisit.getContainer().required = false;
                    break
                case 'Dentist': 
                    this.show(this._lastVisit);
                    this.show(this._lastVisitLabel);
                    this.hide(this._fsPressure);
                    this.hide(this._bmi);
                    this.hide(this._bmiLabel);
                    this.hide(this._pastIllnessesn);
                    this.hide(this._pastIllnessesnLabel);
                    this.hide(this._ageLabel);
                    this.hide(this._age);
                    this._lastVisit.getContainer().required = true;
                    this._fsPressureLow.getContainer().required = false;
                    this._fsPressureHigh.getContainer().required = false;
                    this._bmi.getContainer().required = false;
                    this._pastIllnessesn.getContainer().required = false;
                    this._age.getContainer().required = false;
                    break
                case 'Therapist': 
                    this.show(this._ageLabel);    
                    this.show(this._age);
                    this.hide(this._fsPressure);
                    this.hide(this._bmi);
                    this.hide(this._bmiLabel);
                    this.hide(this._pastIllnessesn);
                    this.hide(this._pastIllnessesnLabel);
                    this.hide(this._lastVisit);
                    this.hide(this._lastVisitLabel);
                    this._age.getContainer().required = true;
                    this._lastVisit.getContainer().required = false;
                    this._fsPressureLow.getContainer().required = false;
                    this._fsPressureHigh.getContainer().required = false;
                    this._bmi.getContainer().required = false;
                    this._pastIllnessesn.getContainer().required = false;
                    break
            }
        }
    }

    _addFormInitvalues(values) {
        const {doctor, purpose, description, fullname, urgency, bmi, pastIllnessesn, age, pressureHigh, pressureLow, lastVisit} = values;
        if (doctor !== 'empty') {
            this._doctor._container.value = doctor;
            this._purpose._container.value = purpose;
            this._description._container.value = description;
            this._urgency._container.value = urgency;
            this._fullName._container.value = fullname;

            switch (doctor) {
                case 'Cardiologist':
                    this._bmi._container.value = bmi;
                    this._pastIllnessesn._container.value = pastIllnessesn;
                    this._age._container.value = age;
                    this._fsPressureLow._container.value = pressureLow;
                    this._fsPressureHigh._container.value = pressureHigh;
                    break
                case 'Dentist':
                    this._lastVisit._container.value = lastVisit;
                    break
                case 'Therapist':
                    this._age._container.value = age;
                    break
            }
        }

    }

    _onCreateBtnClick() {
        if (this.checkFormValidity()) {
            const doctor = this.getControlValue(this._doctor);
            let body = {
                doctor: this.getControlValue(this._doctor), //врач
                purpose: this.getControlValue(this._purpose), //цель визита
                description: this.getControlValue(this._description), //краткое описание визита
                urgency: this.getControlValue(this._urgency), //Срочность
                fullname: this.getControlValue(this._fullName)
            };
            if (doctor === 'Cardiologist') {
                body = {
                    ...body,
                    pressureLow: this.getControl(this._fsPressure).getControlValue(this._fsPressureLow),
                    pressureHigh: this.getControl(this._fsPressure).getControlValue(this._fsPressureHigh),
                    bmi: this.getControlValue(this._bmi),
                    pastIllnessesn: this.getControlValue(this._pastIllnessesn),
                    age: this.getControlValue(this._age),
                }
            }
            if (doctor === 'Dentist') {
                body = {
                    ...body,
                    lastVisit: this.getControlValue(this._lastVisit),
                }
            }
            if (doctor === 'Therapist') {
                body = {
                    ...body,
                    age: this.getControlValue(this._age),
                }
            }

            remoteDataExchange({
                method: 'PUT',
                token: this._token,
                id: this._editInputs.id,
                onSuccess: this._onCardUpdated.bind(this),
                body: JSON.stringify(body)
            })

        }
    }

    close() {
        this.destroy();
        this._onCanceledToApp();
    }

    _onCardUpdated(response) {
        console.log(response);

        remoteDataExchange({
            method: 'GET',
            token: app._data.token,
            onSuccess: app._data.sectionFilter._filterBound
            // onSuccess: app._data.sectionDesk._createDeskUIBound
        })
        this._closeBound();
    }

}

export {EditForm};