import {VisitCard} from "../Form/VisitCard.min.js";

// import {VisitCard} from "../Form/VisitCard.js";

export class VisitCardiologist extends  VisitCard {
    constructor(inputDataObject, appData) {
        super(inputDataObject, appData);
        this._inputs = inputDataObject;
        this._toFillSpecificFields()

    }

    _toFillSpecificFields() {

        this._hiddenFields.insertAdjacentHTML('beforeend', `           

                <div class="${VisitCard.props.class.rowContainer}">
                    <p class="${VisitCard.props.class.rowTitle}">Давление</p>
                    <p class="${VisitCard.props.class.rowText} pressure">${this._inputs.pressureHigh} x ${this._inputs.pressureLow}</p>
                </div>
                <div class="${VisitCard.props.class.rowContainer}">
                    <p class="${VisitCard.props.class.rowTitle}">Индекс массы</p>
                    <p class="${VisitCard.props.class.rowText} bmi">${this._inputs.bmi}</p>
                </div>
                 <div class="${VisitCard.props.class.rowContainer}">
                    <p class="${VisitCard.props.class.rowTitle}">Перенесенные заболевания</p>
                    <p class="${VisitCard.props.class.rowText} pastIllnessesn">${this._inputs.pastIllnessesn}</p>                    
                </div>
                 <div class="${VisitCard.props.class.rowContainer}">
                    <p class="${VisitCard.props.class.rowTitle}">Возраст</p>
                    <p class="${VisitCard.props.class.rowText} age">${this._inputs.age}</p>                    
                </div>
                                  
        
        `)
    }
}
