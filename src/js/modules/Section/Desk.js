import {Component} from "../Base/Component.min.js";
import {Ajax, remoteDataExchange} from "../Base/AJAX.min.js";
import {VisitDentist} from "../Form/VisitDentist.min.js";
import {VisitTherapist} from "../Form/VisitTherapist.min.js";
import {VisitCardiologist} from "../Form/VisitCardiologist.min.js";
import {Drag} from "../Base/Drag.min.js";


// import {Component} from "../Base/Component.js";
// import {Ajax, remoteDataExchange} from "../Base/AJAX.js";
// import {VisitDentist} from "../Form/VisitDentist.js";
// import {VisitTherapist} from "../Form/VisitTherapist.js";
// import {VisitCardiologist} from "../Form/VisitCardiologist.js";
// import {Drag} from "../Base/Drag.js";

class SectionDesk extends Component{
    static props = {
        class: {
            container: 'cardsApp__desk',
        }
    }

    constructor(appData) {
        super();
        this._appData = appData;
        this._createDeskUI();
        this._createDeskUIBound = this._createDeskUI.bind(this);

    }

    _createElement() {
        this._container = document.createElement('section');
        this._container.classList.add(SectionDesk.props.class.container);
    }

    _createDeskUI(arr) {
        if (!arr) {
            this._container.innerHTML = 'No items have been added';
        } else {
            console.log(arr);
            this._container.innerHTML = '';
            arr.forEach(cardObj => {
                this._createSingleCard(cardObj);
            })
        }
    }

    _createSingleCard(card) {
        if(this._container.innerHTML === 'No items have been added') this._container.innerHTML = ''
        let visitCard;
        if (card.doctor === 'Dentist') visitCard = new Drag(new VisitDentist(card, this._appData));
        if (card.doctor === 'Therapist') visitCard = new Drag(new VisitTherapist(card, this._appData));
        if (card.doctor === 'Cardiologist') visitCard = new Drag(new VisitCardiologist(card, this._appData));
        if (card.doctor !== undefined) visitCard.appendTo(this._container);
    }


    getVisits(token) {
        this._token = token;
        remoteDataExchange({
            method: 'GET',
            token,
            onSuccess: this._createDeskUI.bind(this)
        })
    }
}

export {SectionDesk};
