import {Component} from "../Base/Component.min.js";
import {Form} from '../Base/Form.min.js'
import {remoteDataExchange} from "../Base/AJAX.min.js";

// import {Component} from "../Base/Component.js";
// import {Form} from '../Base/Form.js'
// import {remoteDataExchange} from "../Base/AJAX.js";

class SectionFilter extends Component{
    static props = {
        class: {
            container: 'cardsApp__filter',
            title: 'cardsApp__title',
            form: 'cardsApp__form-filter',
            btnSearch: 'cardsApp__btn--search',
        }
    }

    constructor(appData) {
        super();
        this._appData = appData;
        this._boundListeners();
        this._createFilterUI();
    }

    _createElement() {
        this._container = document.createElement('section');
        this._container.classList.add(SectionFilter.props.class.container);
    }

    _createFilterUI() {
        const sectionFilterTitle = document.createElement('h4');
        sectionFilterTitle.classList.add(SectionFilter.props.class.title);
        sectionFilterTitle.innerText = 'Фильтр/Поиск';
        this._container.append(sectionFilterTitle);

        const sectionFilterForm = new Form();
        this._formFilter = sectionFilterForm;
        sectionFilterForm._container.classList.add(SectionFilter.props.class.form)

          this._content = sectionFilterForm.addControl('input', {
            value: '',
            htmlAttr: [
                {   
                    key: 'placeholder',
                    value: 'Поиск по содержимому...'
                },
                {
                    key: 'id',
                    value: 'filterTitle'
                },
                {
                    key: 'name',
                    value: 'filterTitle'
                }
            ],
            listeners: [
                {
                    type: 'input',
                    cb: this._onFilterChangedBound
                },
            ],
        });

        this._urgency = sectionFilterForm.addControl('select', {
                options: [
                    {
                        text: 'Поиск по срочности...',
                        value: 'empty',
                        selected: true
                    },
                    {
                        text: 'Обычная',
                        value: 'regular',
                    },
                    {
                        text: 'Приоритетная',
                        value: 'priority',
                    },
                    {
                        text: 'Неотложная',
                        value: 'urgent',
                    },
                ],
                htmlAttr: [            
                    {
                        key: 'id',
                        value: 'filterUrgency'
                    },
                    {
                        key: 'name',
                        value: 'filterUrgency'
                    }
                ],
                listeners: [
                    {
                        type: 'change',
                        cb: this._onFilterChangedBound
                    },
                ],
        });
        sectionFilterForm.appendTo(this._container);
        this.disableFilter();
        
    }

    disableFilter() {
        this._formFilter.disable(this._content);
        this._formFilter.disable(this._urgency);
    }

    enableFilter() {
        this._formFilter.enable(this._content);
        this._formFilter.enable(this._urgency);
    }

    _boundListeners() {
        this._onFilterChangedBound = this._onFilterChanged.bind(this);
        this._filterBound = this._filter.bind(this);
    }

    _onFilterChanged(event) {
        // if (event.type === 'input' && event.target.value.length < 3)  return

        remoteDataExchange({
            method: 'GET',
            token: this._appData.token, 
            onSuccess: this._filterBound
        })
    }

    _doctorValueTranslate(doctorName, lang) {
        let valueRus;
        if (lang === 'toRus') {
            switch (doctorName) {
                case 'Cardiologist':
                    valueRus = 'Кардиолог';
                    break
                case 'Dentist':
                    valueRus = 'Стоматолог';
                    break
                case 'Therapist':
                    valueRus = 'Терапевт';
                    break
            }
        } else if (lang === 'toEng') {
            switch (doctorName) {
                case 'Кардиолог':
                    valueRus = 'Cardiologist';
                    break
                case 'Стоматолог':
                    valueRus = 'Dentist';
                    break
                case 'Терапевт':
                    valueRus = 'Therapist';
                    break
            }
        }
        return valueRus;
    }

    _translateArr(arr, lang) {
        return [...arr].map(card => {
            return this._textFormat(card, lang);
        });
    }

    _textFormat(obj, lang) {
        return {
            ...obj,
            doctor: this._doctorValueTranslate(obj.doctor, lang)
        };
    }

    _toLowerCase(arr) {
        const arry = [...arr]
        arry.forEach(card => {
            for (let key in card) {
                if (typeof(card[key]) === 'string') card[key] = card[key].toLowerCase()
            }
        })
        return arry
    }

    _filter(cards) {
        const data = {
            content: this._formFilter.getControlValue(this._content).toLowerCase(),
            urgency: this._formFilter.getControlValue(this._urgency)
        }

        let rusCards = this._toLowerCase(this._translateArr(cards, 'toRus'));
        let cardIndex = [];

        rusCards.forEach((card, index) => {
            if (data.urgency !== 'empty' && data.urgency !== card.urgency) return;
            if (data.content.length > 0) {
                for (let key in card) {
                    if (card[key].includes(data.content)) {
                        cardIndex.push(index);
                    }
                }
                return
            }
            cardIndex.push(index);
        })

        let filteredCards = cards.filter((card, index) => {
           return cardIndex.includes(index)
        })
        this._appData.sectionDesk._createDeskUI(filteredCards);
    }
}

export {SectionFilter};