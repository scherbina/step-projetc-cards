import {Component} from "../Base/Component.min.js";
import {Modal} from "../Base/Modal.min.js";
import {LoginForm} from "../Form/Login.min.js";
import {CreateForm} from "../Form/Create.min.js";
import {Form} from "../Base/Form.min.js"
import {remoteDataExchange} from "../Base/AJAX.min.js";


// import {Component} from "../Base/Component.js";
// import {Modal} from "../Base/Modal.js";
// import {LoginForm} from "../Form/Login.js";
// import {CreateForm} from "../Form/Create.js"
// import {Form} from "../Base/Form.js"

class SectionHeader extends Component{
    static props = {
        logoURL: './img/header/logo.png',
        // loginText: 'Welcome, ',
        class: {
            container: 'cardsApp__header',
            logo: 'cardsApp__logo',
            logoImg: 'cardsApp__logo--img',
            login: 'cardsApp__header-login',
            btnLogin: 'cardsApp__btn--login',
            btnCreate: 'cardsApp__btn--create',
        }
    }

    constructor(appData) {
        super();
        this._appData = appData;
        this._boundListeners();
        this._createHeaderUI();

        const token = this._appData.token;

        if (token) {
            remoteDataExchange({
                method: 'GET',
                token,
                onSuccess: this._onGetCardsSuccessBound,
                onError: this._onGetCardsErrorBound
            })
        } 

    }

    _createElement() {
        this._container = document.createElement('section');
        this._container.classList.add(SectionHeader.props.class.container);
    }

    _createHeaderUI() {
        //Add Logo
        const sectionHeaderLogo = document.createElement('div');
        sectionHeaderLogo.classList.add(SectionHeader.props.class.logo);
        const sectionHeaderImg = document.createElement('img');
        sectionHeaderImg.classList.add(SectionHeader.props.class.logoImg);
        sectionHeaderImg.src = SectionHeader.props.logoURL;
        sectionHeaderLogo.append(sectionHeaderImg);
        this._container.append(sectionHeaderLogo);

        //Add Buttons: Login, Create
        this._sectionHeaderLogin = new Form();
        this._sectionHeaderLogin.getContainer().classList.add(SectionHeader.props.class.login);
        
        this._sectionHeaderBtnLogin = this._sectionHeaderLogin.addControl('button', {
            text: 'Вход',
            listeners: [
                {
                    type: 'click',
                    cb: this._onLoginBtnClickBound
                },
            ],
            class: [SectionHeader.props.class.btnLogin]
        });
        this._sectionHeaderBtnCreate = this._sectionHeaderLogin.addControl('button', {
            text: 'Создать визит',
            listeners: [
                {
                    type: 'click',
                    cb: this._onCreateBtnClickBound
                },
            ],
            class: [SectionHeader.props.class.btnCreate, 'hide']
        });
        this._sectionHeaderLogin.appendTo(this._container);

    }

    _boundListeners() {
        this._onLoginBtnClickBound = this._onLoginBtnClick.bind(this);
        this._onCreateBtnClickBound = this._onCreateBtnClick.bind(this);
        this._onLoginSuccessBound = this._onLoginSuccess.bind(this);
        this._onGetCardsSuccessBound = this._onGetCardsSuccess.bind(this);
        this._onGetCardsErrorBound = this._onGetCardsError.bind(this);
        this._onLoginCancelBound = this._onLoginCancel.bind(this);
        this._onCardCreatedBound = this._onCardCreated.bind(this);
        this._onCreateFormClosedBound = this._onCreateFormClosed.bind(this);
    }

    _onLoginBtnClick() {
        this._sectionHeaderLogin.disable(this._sectionHeaderBtnLogin);
        this._loginForm = new Modal (
            new LoginForm(this._onLoginSuccessBound, this._onLoginCancelBound)
        );
        this._loginForm.open();
    }

    _onCreateBtnClick() {
        this._sectionHeaderLogin.disable(this._sectionHeaderBtnCreate);
        this._createForm = new Modal (
            new CreateForm(this._appData.token, this._onCardCreatedBound, this._onCreateFormClosedBound)
        );
        this._createForm.open();
    }

    _onLoginSuccess({token}) {
        this._sectionHeaderLogin.hide(this._sectionHeaderBtnLogin);
        this._sectionHeaderLogin.show(this._sectionHeaderBtnCreate);
        this._loginForm.destroy();
        this._appData.onLoginSuccess(token);
    }

    _onGetCardsSuccess(response) {
        this._sectionHeaderLogin.hide(this._sectionHeaderBtnLogin);
        this._sectionHeaderLogin.show(this._sectionHeaderBtnCreate);
        this._appData.onGetCardsSuccess(response);
    }

    _onGetCardsError(response) {
        console.log('Token expired');
    }

    _onLoginCancel() {
        this._loginForm.destroy();
        this._sectionHeaderLogin.enable(this._sectionHeaderBtnLogin);
    }

    _onCardCreated(card) {
        this._createForm.destroy();
        this._sectionHeaderLogin.enable(this._sectionHeaderBtnCreate);
        this._appData.onCardCreated(card)
    }

    _onCreateFormClosed() {
        this._createForm.destroy();
        this._sectionHeaderLogin.enable(this._sectionHeaderBtnCreate);
    }
}

export {SectionHeader};