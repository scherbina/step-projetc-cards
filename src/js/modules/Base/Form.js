import {Component} from "./Component.min.js";
import * as FormControl from "./FormControls.min.js";

// import {Component} from "./Component.js";
// import * as FormControl from "./FormControls.js";


class Form extends Component {
    constructor() {
        super();
        this._controls = [];
        this._container = document.createElement('form');
        
        this._onSubmitBound = this._onSubmit.bind(this);
        this._container.addEventListener('submit', this._onSubmitBound);
    }

    addControl(type, initialValue) {
        let control;
        switch (type) {
            case 'input': control = new FormControl.InputComponent(initialValue);
                break;
            case 'checkbox': control = new FormControl.CheckboxComponent(initialValue);
                break;
            case 'select': control = new FormControl.SelectComponent(initialValue);
                break
            case 'label': control = new FormControl.LabelComponent(initialValue);
                break
            case 'button': control = new FormControl.ButtonComponent(initialValue);
                break
            case 'textarea': control = new FormControl.TextAreaComponent(initialValue);
                break
            case 'fieldset': control = new FormControl.FieldsetComponent(initialValue);
                break
        }
        this._controls.push(control);
        control.appendTo(this._container);
        return control
    }

    getControl(control) {
        return this._controls.find(controlItem => {
            return controlItem === control});
    }

    getControlValue(control) {
        return this.getControl(control).getValue();
    }

    show(control) {
        this.getControl(control).getContainer().classList.remove('hide');
    }

    hide(control) {
        this.getControl(control).getContainer().classList.add('hide');
    }

    disable(control) {
        this.getControl(control).getContainer().disabled = true;
    }

    enable(control) {
        this.getControl(control).getContainer().disabled = false;
    }

    checkValidityForControl(control) {
        const controlElement = this.getControl(control).getContainer();
        if (controlElement.tagName !== 'LABEL') return controlElement.checkValidity();
    }

    checkFormValidity() {
        let isValid = true;
        this._controls.forEach(control => {
            const controlElement = this.getControl(control).getContainer();
            if (controlElement.tagName !== 'LABEL') return isValid = isValid && controlElement.checkValidity()
        })
        return isValid;
    }

    _onSubmit(e) {
        e.preventDefault();
    }

    destroy() {
        this._controls.forEach(control => control.destroy());
        this._container.removeEventListener('submit', this._onSubmitBound);
        this._container.remove();
    }
}

export {Form};