export {FormControl} from './FormControl/FormControl.min.js';
export {InputComponent} from './FormControl/InputComponent.min.js';
export {CheckboxComponent} from './FormControl/CheckBoxComponent.min.js';
export {SelectComponent} from './FormControl/SelectComponent.min.js';
export {LabelComponent} from './FormControl/LabelComponent.min.js';
export {ButtonComponent} from './FormControl/ButtonComponent.min.js';
export {TextAreaComponent} from './FormControl/TextAreaComponent.min.js';
export {FieldsetComponent} from './FormControl/FieldsetComponent.min.js';


// export {FormControl} from './FormControl/FormControl.js';
// export {InputComponent} from './FormControl/InputComponent.js';
// export {CheckboxComponent} from './FormControl/CheckBoxComponent.js';
// export {SelectComponent} from './FormControl/SelectComponent.js';
// export {LabelComponent} from './FormControl/LabelComponent.js';
// export {ButtonComponent} from './FormControl/ButtonComponent.js';
// export {TextAreaComponent} from './FormControl/TextAreaComponent.js';
// export {FieldsetComponent} from './FormControl/FieldsetComponent.js';