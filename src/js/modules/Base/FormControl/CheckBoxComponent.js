import {FormControl} from "./FormControl.min.js";

// import {FormControl} from "./FormControl.js";

class CheckboxComponent extends FormControl {
    //initialValue: true => checked or false => unchecked
    constructor(initialValue) {
        super(initialValue);
    }
    _createElement() {
        this._container = document.createElement('input');
        this._container.type = 'checkbox';
    }

    getValue() {
        return this._container.checked;
    }

    setValue(checked) {
        checked ? this._container.checked = true : this._container.checked = false;
    }
}

export {CheckboxComponent};