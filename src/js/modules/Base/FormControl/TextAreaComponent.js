import {FormControl} from "./FormControl.min.js";

// import {FormControl} from "./FormControl.js";

class TextAreaComponent extends FormControl {
    constructor(initialValue) {
        super(initialValue);
    }
    _createElement() {
        this._container = document.createElement('textarea');
    }

}

export {TextAreaComponent};