import {FormControl} from "./FormControl.min.js";

// import {FormControl} from "./FormControl.js";

class ButtonComponent extends FormControl {
    constructor(initialValue) {
        super(initialValue);
        
    }
    _createElement() {
        this._container = document.createElement('button');
        this._container.classList.add('cardsApp__btn');
    }

    setValue({text}) {
        this._container.innerText = text;
    }
}

export {ButtonComponent};