import {FormControl} from "./FormControl.min.js";

// import {FormControl} from "./FormControl.js";

class LabelComponent extends FormControl {
    //initialValue: Object
    //   {
    //         text: 'text',
    //         htmlFor: 'id',
    //    }
    constructor(InitialValue) {
        super(InitialValue);
    }
    _createElement() {
        this._container = document.createElement('label');
    }

    //Возвращает объект с текстом подписи и ID элемента для которого подпись создана
    getValue() {
        return {text: this._container.value, for: this._container.htmlFor}
    }

    
    setValue({text, htmlFor}) { 
        this._container.innerText = text;
        this._container.htmlFor = htmlFor;
    }
}

export {LabelComponent};