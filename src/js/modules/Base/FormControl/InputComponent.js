import {FormControl} from "./FormControl.min.js";

// import {FormControl} from "./FormControl.js";

class InputComponent extends FormControl {
    constructor(initialValue) {
        super(initialValue);
    }
    _createElement() {
        this._container = document.createElement('input');
    }

}

export {InputComponent};