import {Component} from "../Component.min.js";

// import {Component} from "../Component.js";

class FormControl extends Component {
    //initialValue: Object
    //   {
    //         text: 'text',
    //         label: 'wrapper label inner text'
    //         htmlAttr: Array of Objects {key: attrName, value};
    //         listeners: Array of Objects {onClick: handler();}
    //    }
    constructor(initialValue) {
        super();
        this.setValue(initialValue);
        if (initialValue.htmlAttr) this.setHtmlAttr(initialValue.htmlAttr);
        if (initialValue.label) this._wrapInLabel(initialValue.label);
        if (initialValue.class) this.addClass(initialValue.class);
        if (initialValue.listeners) {
            this._listeners = initialValue.listeners;
            this._addListeners();
        } else this._listeners = [];
    }

    setValue(value) {
        value ? this._container.value = value.value :  this._container.value = '';
    }

    getValue() {
        return this._container.value;
    }

    setHtmlAttr(attributes) {
        attributes.forEach(({key, value}) => {
            this._container.setAttribute(key, value)
        })
    }

    _wrapInLabel(label) {
        const labelEl = document.createElement('label');
        labelEl.innerText = label.text;
        labelEl.append(this._container);
        this._container = labelEl;
    }

    addClass(arrClass) {
        arrClass.forEach(className => this._container.classList.add(className))
    }

    removeClass(arrClass) {
        arrClass.forEach(className => this._container.classList.remove(className))
    }

    _addListeners() {
        this._listeners.forEach(({type, cb}) => this._container.addEventListener(type, cb))
    }

    _removeListeners() {
        this._listeners.forEach(({type, cb}) => this._container.removeEventListener(type, cb))
    }

}

export {FormControl};
