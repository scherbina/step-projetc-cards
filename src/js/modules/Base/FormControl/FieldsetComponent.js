import {Component} from "../Component.min.js";

// import {Component} from "../Component.js";

class FieldsetComponent extends Component {
    constructor(InitialValue) {
        super(InitialValue);
        this._controls = [];
        this.setLegend(InitialValue.text);
    }
    _createElement() {
        this._container = document.createElement('fieldset');
        this._legend = document.createElement('legend');
        this._container.append(this._legend);
    }

    setLegend(text) { 
        this._legend.innerText = text;
    }

    addControl(control) {
        this._controls.push(control);
        control.appendTo(this._container);
        return control
    }

    getControl(control) {
        return this._controls.find(controlItem => controlItem === control);
    }

    getControlValue(control) {
        return this.getControl(control).getValue();
    }

    destroy() {
        this._controls.forEach(control => control.destroy());
        this._container.remove();
    }

}

export {FieldsetComponent};