import {FormControl} from "./FormControl.min.js";

// import {FormControl} from "./FormControl.js";

class SelectComponent extends FormControl {
    //initialValue: Object
    // {
    //   options: [
            // {
    //         text: 'text',
    //         value: 'value',
    //         selected: true or false
    //          }
    //    ]
    // }
    constructor(InitialValue) {
        super(InitialValue);
    }
    _createElement() {
        this._container = document.createElement('select');
    }

    //Возвращает индекс выбранной опции SELECT
    getSelectedID() {
        return this._container.options.selectedIndex
    }

    //Возвращает объект с значениями выбранной опции SELECT
    getValue() {
        const selectedOption = this._container.options[this.getSelectedID()];
        return selectedOption.value
    }

    //Задать опции для SELECT
    setValue({options}) { 
        options.forEach( option => this.addOption(option))
    }

    //Добавить опцию к SELECT
    //Первый аргумент: {
    //         text: 'text',
    //         value: 'value',
    //         selected: true or false
    //    }
    //Второй аргумент: индекс опции, перед которой надо вставить новую. Если не передано, то вставляется в конце
    addOption({text, value, selected = false}, beforeOptionID) {
        const newOption = new Option(text, value);
        this._container.options.add(newOption, beforeOptionID);
        if (beforeOptionID) {
            this._container.options[beforeOptionID].selected = selected
        } else {
            this._container.options[this._container.options.length-1].selected = selected
        } 
    }
}

export {SelectComponent};