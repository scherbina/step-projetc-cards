import {Component} from "./Component.min.js";

// import {Component} from "./Component.js";


class Drag extends Component {
    constructor(component) {
        super();
        this._component = component;
        this.open();
        this.addListeners();
    }


    open() {
        this._container = document.createElement('div');
        this._container.classList.add('draggable');
        this._container.setAttribute("draggable", "true");
        this._container.style.cursor = "move";


        this._component.appendTo(this._container);
        this.appendTo(document.body);
    }

    addListeners() {
        const draggables = document.querySelectorAll('.draggable');
        draggables.forEach(draggable => {
            draggable.addEventListener('dragstart', () =>{
                draggable.classList.add('dragging');

            });
            draggable.addEventListener('dragend', () =>{
                draggable.classList.remove('dragging');

            })
        });


        const dropZone = document.querySelector('.cardsApp__desk');

        dropZone.addEventListener('dragover', (e) => {
            e.preventDefault();
            const afterContainer = this.dragAfterContainer(e.clientY);

            const draggable = document.querySelector('.dragging');

            if(afterContainer == null){
                dropZone.appendChild(draggable);
            }else {
                dropZone.insertBefore(draggable, afterContainer);
            }

        });
    }

    dragAfterContainer(y) {
        const draggableElements = [...document.querySelectorAll('.draggable:not(.dragging)')];

        return draggableElements.reduce((closest, child) => {
            const box = child.getBoundingClientRect();
            const offset = y - box.top - box.height/2;

            if(offset < 0 && offset > closest.offset){
                return { offset: offset, element: child}
            }
            else {
                return closest
            }

        }, { offset: Number.NEGATIVE_INFINITY}).element
    }


    destroy() {
        this._component.destroy();
        this._container.remove();
    }

    getComponentContainer() {
        return this._component.getContainer();
    }
}

export {Drag};