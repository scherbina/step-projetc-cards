import {Component} from "./Component.min.js";

// import {Component} from "./Component.js";


class Modal extends Component {
    constructor(component) {
        super();
        this._component = component;
        this._boundListeners();
    }

    _boundListeners() {
        this._onClickBound = this._onClick.bind(this);
    }

    open() {
        this._container = document.createElement('div');
        this._container.classList.add('modal');
        this._contentContainer = document.createElement('div');
        this._contentContainer.classList.add('modal-content');
        this._container.append(this._contentContainer);
        this._component.appendTo(this._contentContainer);
        this.appendTo(document.body);
        this._container.addEventListener('click', this._onClickBound);
    }

    close() {
        this._component.close();
        this.destroy();
    }

    getComponentContainer() {
        return this._component.getContainer();
    }

    _onClick(e) {
        if (e.target.className === 'modal') this.close();
    }

    _removeListeners() {
        this._container.removeEventListener('click', this._onClickBound);
    }
}

export {Modal};