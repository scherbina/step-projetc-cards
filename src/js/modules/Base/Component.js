class Component {
    _container;

    constructor() {
        this._createElement();
    }

    _createElement() {
    }

    getContainer() {
        return this._container
    }

    appendTo(parent) {
        parent.append(this._container);
    }

    _removeListeners() {
    }

    destroy() {
        this._removeListeners();
        this._container.remove();
    }

}

export {Component};