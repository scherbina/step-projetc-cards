class Ajax {
    constructor({url, method, body, token}) {
        this._options = {
            method: method,
            body: body,
            headers: {
                'Content-Type': 'application/json'
            }    

        }

        if (token) this._options = {
            ...this._options,
            headers: {
                'Authorization': `Bearer ${token}`,
            }    
        }

        this._url = url;
    }

    open() {
        return fetch(this._url, this._options)
        // .then(response => {
        //     // console.log(response);
        //     // if (!response.ok) {throw response.message};
        //     // return response.json();
        // })
    }
}

function remoteDataExchange(options) {
    const urlCards = 'http://cards.danit.com.ua/cards';
    const urlLogin = 'http://cards.danit.com.ua/login';
    let url;
    let login;

    if (options.token) {
        url = urlCards;
        if (options.id) url = `${url}/${options.id}`;
    } else {
        url = urlLogin;
        login = true;
        if (!options.body && !options.body.email && !options.body.password) {
            throw ('Request without Token and Authorization data')
            return
        }
    }
    options = { ...options, url };
    let ajax = new Ajax(options);

    ajax.open()
        .then(response => {
            console.log(response);
            console.log(options);
            if (response.ok) {
                response.json()
                .then(response => {
                    switch (options.method) {
                        case 'GET':
                            if (response.length > 0) options.onSuccess(response)
                            break
                        case 'DELETE':
                            if (response.status === 'Success') {
                                options.onSuccess(response)
                                break
                            } else {
                                alert(`delete faild with status: ${response.status}
                                        Try Again
                                    `)
                            }
                            break
                        case 'POST':
                            if (login) {
                                if (response.status === 'Success') {
                                    options.onSuccess(response)
                                } else {
                                    alert(response.message)
                                }
                                break
                            } else {
                                options.onSuccess(response)
                                break
                            }
                        case 'PUT':
                            console.log(response);
                            options.onSuccess(response)
                            break
                    }
                })
            } else options.onError(response);
            
        });
}

export {Ajax, remoteDataExchange}